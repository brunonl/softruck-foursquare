# Front-end Test Softruck

## Getting Started

This file contains all the instructions for running the project in your machine.

### Installing

Only execute the commands as below:

```
npm install

gulp
```

## Running the optimization tasks

For run the project optimization tasks run

```
gulp build
```

Set the baseDir to dist
```
gulp.task('server', function() {
	browserSync.init({
		server: {
			baseDir: 'app'  --> set to "dist"
		}
		});
	});
```

run "gulp" command again

## Authors

* **Bruno Meireles**


