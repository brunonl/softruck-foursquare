angular.module("softruckFoursquare").controller("homeCtrl", function ($scope,venuesAPI,categoryList) {

	var vm = $scope;
	vm.formSearch = [];
	vm.openedMenu = true;
	vm.turnBtnHeatMap = false;

	//--->Triggers
	vm.triggerMenu = function() {
		vm.openedMenu = !vm.openedMenu;
	}
	
	//--->Categorys
	vm.listCategory = [];
	fomatCategorys(categoryList.data);
	/*Poderia fazer aqui uma função recursiva para varrer todo o objeto, mas vi que até o 
	segundo nível de arrays já é o suficiente para varrer uma boa gama de categorias.*/
	function fomatCategorys (list) {
		list.response.categories.forEach(function (subcategoria) {
			subcategoria.categories.forEach(function(categoria) {
				vm.listCategory.push(categoria);
			})
		})
	}

	//--->Map
	vm.infoWindow = new google.maps.InfoWindow;
	vm.heatmap = new google.maps.visualization.HeatmapLayer;
	vm.mapOptions = {
		zoom: 8,
		center: {lat: -34.397, lng: 150.644},
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}

	vm.fnLoadMap = function () {
		vm.map = new google.maps.Map(document.getElementById('map'), vm.mapOptions);
		vm.fnGeolocation();
	}

	vm.fnLoadHeatMap = function () {
		clearHeatMapPoints();
		vm.heatmap = new google.maps.visualization.HeatmapLayer({
			data: vm.heatMapPoints,
			map: vm.map,
			radius: 55
		});

	}

	function createHeatMapPoints(listRecommendVenues) {
		vm.heatLayerToggle = true;
		vm.turnBtnHeatMap = true;
		vm.heatMapPoints = [];
		listRecommendVenues.forEach(function(element) {
			vm.heatMapPoints.push(new google.maps.LatLng(element.venue.location.lat, element.venue.location.lng));
		});
	}

	function clearHeatMapPoints() {
		vm.heatmap.setMap(null);
	} 

	vm.toggleHeatMap = function() {
		vm.heatLayerToggle = !vm.heatLayerToggle;
		vm.heatmap.setMap(vm.heatmap.getMap() ? null : vm.map);
	}

	//--->API
	vm.recommendPlaces;
	vm.searhRecommendVenues = function() {
		venuesAPI.getRecommendationsVenues(vm.actualPosition,vm.formSearch.selectedCategory.originalObject.name,vm.formSearch.distanceSearch)
		.then(function(success) {
			vm.recommendPlaces = success.data.response.groups[0].items;
			console.log(vm.recommendPlaces);
			if(!emptyRecommendPlaces(vm.recommendPlaces)){
				createMarkersRecommendVenues(vm.recommendPlaces);
				createHeatMapPoints(vm.recommendPlaces);
				vm.fnLoadHeatMap();
			}	
		},function(erro) {
			console.log("Erro: "+erro);
		})
	}

	function emptyRecommendPlaces(list) {
		if(list.length == 0) {
			vm.emptyRecommentValues = true;
			vm.emptyRecommentValuesMessge = "Ops: Não há resultados para os parametros selecionados, tente aumentar a distância.";
			return true;
		} else {
			vm.emptyRecommentValues = false;
			return false;
		}
	}

	//--->Geolocation
	vm.fnGeolocation = function () {
		let image = "/img/pin.png";
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				vm.actualPosition = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};

				createMarkers(vm.actualPosition,image,null,false); 

				vm.map.setZoom(13);
				vm.map.setCenter(vm.actualPosition);

			}, function() {
				fnHandleLocationError(true, vm.infoWindow, vm.map.getCenter());
			});
		} else {
			fnHandleLocationError(false, vm.infoWindow, vm.map.getCenter());
		}
	}

	function fnHandleLocationError(browserHasGeolocation, infoWindow, pos) {
		vm.infoWindow.setPosition(pos);
		vm.infoWindow.setContent(browserHasGeolocation ?
			'Ops: O serviço de Geolocalização falhou, tente ativar seu local no navigador.' :
			'Ops: Seu browser não suporta Geolocalização.');
		vm.infoWindow.open(vm.map);
	}

	//--->Markers
	vm.markers = [];
	function createMarkersRecommendVenues(listRecommendVenues) {
		deleteMarkers();
		listRecommendVenues.forEach(function(element) {
			let image = element.venue.categories[0].icon.prefix + "bg_32.png";
			createMarkers(element.venue.location, image, element.venue, true)
			if(vm.formSearch.distanceSearch > 50000) {
				vm.map.setZoom(12);
			}
		});
	}

	function createMarkers(pos,img,element,infoWindow) {
		let marker = new google.maps.Marker({
			map: vm.map,
			position:  {
				lat: pos.lat,
				lng: pos.lng
			},
			icon: img
		});
		
		if(infoWindow) {
			let contentString = 'Local: ' + element.name + '<br>' +
			'Categoria: ' + element.categories[0].name;

			let infowindow = new google.maps.InfoWindow({
				content: contentString
			});

			marker.addListener('click', function() {
				infowindow.open(marker.get('map'), marker);
			});
		}

		vm.markers.push(marker);
	}

	function setMapOnAll(map) {
		for (var i = 0; i < vm.markers.length; i++) {
			vm.markers[i].setMap(map);
		}
	}

	function clearMarkers() {
		setMapOnAll(null);
	}

	function deleteMarkers() {
		clearMarkers();
		vm.markers = [];
	}
})