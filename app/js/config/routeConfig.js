angular.module("softruckFoursquare").config(function ($routeProvider) {
	$routeProvider.when("/login", {
		templateUrl: "view/login/login.html",
		controller: "loginCtrl"
	});

	$routeProvider.when("/home", {
		templateUrl: "view/home/home.html",
		controller: "homeCtrl",
		resolve: {
			categoryList: function (venuesAPI) {
				return venuesAPI.getCategory();
			}
		}
	});
	
	$routeProvider.otherwise({redirectTo: "/login"});
});