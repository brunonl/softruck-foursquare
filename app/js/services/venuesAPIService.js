angular.module("softruckFoursquare").factory("venuesAPI", function ($http, config) {

	var _getCategory = function () {
		let url = config.baseUrl + 
				  "/venues/categories/?" +
				  "client_id=" + config.client_id +
				  "&client_secret=" + config.client_secret +
				  "&v=" + config.v;
		return $http.get(url);
	};

	var _getRecommendationsVenues = function (actualLocation,nameCategory,distance) {
		let url = config.baseUrl +
				  "/venues/explore/?ll=" + actualLocation.lat + "," +actualLocation.lng +
				  "&query=" + nameCategory +
				  "&radius=" + distance +
				  "&client_id=" + config.client_id +
				  "&client_secret=" + config.client_secret +
				  "&v=" + config.v;
		return $http.get(url);
	};

	return {
		getRecommendationsVenues: _getRecommendationsVenues,
		getCategory: _getCategory,
	};
});